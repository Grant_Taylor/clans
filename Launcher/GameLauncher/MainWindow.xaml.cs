﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Windows;
using System.Windows.Controls;

namespace GameLauncher
{
    enum LauncherStatus
    {
        ready,
        failed,
        current,
        downloadingUpdater,
        downloadingGame,
        downloadingUpdate,
    }
    public partial class MainWindow : Window
    {
        private string RootDirectory;

        public static Version _Ver;

        string VersionAPI = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3QvcyFBakl6RkhpenBQUWhqS2xkakY4ZHZxMmp0aXpiUEE_ZT1wcDkxVjE/root/content";
        string LauncherVersionAPI = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3QvcyFBakl6RkhpenBQUWhqTFZwYmk5Ym1KNlZZRkdCQ2c_ZT1TQWZwbm0/root/content";
        string UpdaterAPIWindows = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBakl6RkhpenBQUWhqLVlTaE1iLTNiVEFaUGJ4dGc_ZT1oQ2ZKSTI/root/content";
        string UpdaterAPILinux = "NON-WORKING";
        string DownloadAPIWindows = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBakl6RkhpenBQUWhqS2xmdVlsMjZqai1tSXo4a3c_ZT1vSDcyb3Q/root/content";
        string DownloadAPILinux = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3UvcyFBakl6RkhpenBQUWhqS2xoa0JVaG42dnBhYkRGd3c_ZT1HS2VZMEw/root/content";
        string ConnectionSettingsAPI = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3QvcyFBakl6RkhpenBQUWhqS2xlVjJuX2dhYzhxTzJGeHc_ZT1HRThOTXM/root/content";

        string LauncherVersionFile = "LauncherVersion.txt";
        string GameVersionFile = "Version.txt";
        string ConnectionSettingsFile = "Client/ConnectionSettings.txt";
        string UpdaterFile = "LaunchUpdater.exe";
        string BuildFile = "Build.zip";
        string GameFile = "Client/Clans.exe";

        private LauncherStatus _Status;
        internal LauncherStatus Status
        {
            get => _Status;
            set
            {
                _Status = value;
                switch (_Status)
                {
                    case LauncherStatus.ready:
                        ContextButton.Content = "Play";
                        break;
                    case LauncherStatus.failed:
                        ContextButton.Content = "Retry";
                        break;
                    case LauncherStatus.current:
                        ContextButton.Content = "Pending";
                        break;
                    case LauncherStatus.downloadingUpdater:
                        ContextButton.Content = "Downloading";
                        break;
                    case LauncherStatus.downloadingGame:
                        ContextButton.Content = "Downloading";
                        break;
                    case LauncherStatus.downloadingUpdate:
                        ContextButton.Content = "Updating";
                        break;
                }
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            //Linux
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                RootDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            }
            //Windows
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                RootDirectory = Directory.GetCurrentDirectory();
            }

            LauncherVersionFile = Path.Combine(RootDirectory, LauncherVersionFile);
            GameVersionFile = Path.Combine(RootDirectory, GameVersionFile);
            ConnectionSettingsFile = Path.Combine(RootDirectory, ConnectionSettingsFile);
            UpdaterFile = Path.Combine(RootDirectory, UpdaterFile);
            BuildFile = Path.Combine(RootDirectory, BuildFile);
            GameFile = Path.Combine(RootDirectory, GameFile);

            CheckLauncherVersion();
            CheckForUpdates();
        }

        public void CheckLauncherVersion()
        {
            if (File.Exists(LauncherVersionFile))
            {
                _Ver = new GameLauncher.Version(File.ReadAllText(LauncherVersionFile));
                LauncherVersionText.Text = "Launcher Version: " + _Ver.ToString();
                try
                {
                    WebClient webClient = new WebClient();
                    Version onlineVersion = new Version(webClient.DownloadString(LauncherVersionAPI));

                    if (onlineVersion.IsDifferentThan(_Ver))
                    {
                        InstallUpdater(false, onlineVersion);
                    }
                    else
                    {
                        Status = LauncherStatus.current;
                        if (File.Exists(UpdaterFile))
                        {
                            File.Delete(UpdaterFile);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Status = LauncherStatus.failed;
                    Console.WriteLine($"Failed to download content: {ex}");

                }
            }
            else
            {
                InstallUpdater(false, Version.zero);
            }
        }

        public void CheckForUpdates()
        {
            if (File.Exists(GameVersionFile))
            {
                _Ver = new GameLauncher.Version(File.ReadAllText(GameVersionFile));
                GameVersionText.Text = "Game Version: " + _Ver.ToString();
                try
                {
                    WebClient webClient = new WebClient();
                    Version onlineVersion = new Version(webClient.DownloadString(VersionAPI));

                    webClient.DownloadFile(new Uri(ConnectionSettingsAPI), ConnectionSettingsFile);

                    if (onlineVersion.IsDifferentThan(_Ver))
                    {
                        InstallGameFiles(false, onlineVersion);
                    }
                    else
                    {
                        Status = LauncherStatus.ready;
                    }
                }
                catch (Exception ex)
                {
                    Status = LauncherStatus.failed;
                    Console.WriteLine($"Failed to download content: {ex}");

                }
            }
            else
            {
                InstallGameFiles(false, Version.zero);
            }
        }

        private void InstallUpdater(bool _IsUpdate, Version _OnlineVersion)
        {
            try
            {
                WebClient webClient = new WebClient();
                if (_IsUpdate)
                {
                    Status = LauncherStatus.downloadingUpdate;
                }
                else
                {
                    Status = LauncherStatus.downloadingUpdater;
                    _OnlineVersion = new Version(webClient.DownloadString(LauncherVersionAPI));
                }

                //Linux
                if (Environment.OSVersion.Platform == PlatformID.Unix)
                {
                    Console.WriteLine("Downloading Linux Version");
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadUpdaterCompleteCallBack);
                    //webClient.DownloadFileAsync(new Uri(UpdaterAPILinux), RootDirectory + "/Build.zip", _OnlineVersion);
                }
                //Windows
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    Console.WriteLine("Downloading Windows Version");
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadUpdaterCompleteCallBack);
                    webClient.DownloadFileAsync(new Uri(UpdaterAPIWindows), UpdaterFile, _OnlineVersion);
                }
            }
            catch (Exception ex)
            {
                Status = LauncherStatus.failed;
                Console.WriteLine($"{ex}");
                //Output some form of message to the user

            }
        }

        private void InstallGameFiles(bool _IsUpdate, Version _OnlineVersion)
        {
            try
            {
                WebClient webClient = new WebClient();
                if (_IsUpdate)
                {
                    Status = LauncherStatus.downloadingUpdate;
                }
                else
                {
                    Status = LauncherStatus.downloadingGame;
                    _OnlineVersion = new Version(webClient.DownloadString(VersionAPI));
                }

                //Linux
                if (Environment.OSVersion.Platform == PlatformID.Unix)
                {
                    Console.WriteLine("Downloading Linux Version");
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompleteCallBack);
                    webClient.DownloadFileAsync(new Uri(DownloadAPILinux), BuildFile, _OnlineVersion);
                }
                //Windows
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    Console.WriteLine("Downloading Windows Version");
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompleteCallBack);
                    webClient.DownloadFileAsync(new Uri(DownloadAPIWindows), BuildFile, _OnlineVersion);
                }
            }
            catch (Exception ex)
            {
                Status = LauncherStatus.failed;
                Console.WriteLine($"{ex}");
                //Output some form of message to the user

            }
        }

        private void DownloadUpdaterCompleteCallBack(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("DownloadCanceled");
                Environment.Exit(1);
            }
            if (e.Error != null)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(0);
            }
            try
            {
                string onlineVersion = ((Version)e.UserState).ToString();

                File.WriteAllText(LauncherVersionFile, onlineVersion);
                LauncherVersionText.Text = onlineVersion;

                System.Diagnostics.Process.Start(UpdaterFile);
                Environment.Exit(0);

            }
            catch (Exception ex)
            {
                Status = LauncherStatus.failed;
                Console.WriteLine($"{ex}");
                //Output some form of message to the user
            }
        }

        private void DownloadGameCompleteCallBack(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("DownloadCanceled");
                Environment.Exit(1);
            }
            if (e.Error != null)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(0);
            }
            try
            {
                string onlineVersion = ((Version)e.UserState).ToString();
                ZipFile.ExtractToDirectory(BuildFile, RootDirectory);
                File.Delete(BuildFile);

                File.WriteAllText(GameVersionFile, onlineVersion);
                GameVersionText.Text = "Game version: " + onlineVersion;
                Status = LauncherStatus.ready;
            }
            catch (Exception ex)
            {
                Status = LauncherStatus.failed;
                Console.WriteLine($"{ex}");
                //Output some form of message to the user
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            CheckLauncherVersion();
            CheckForUpdates();
        }

        void ContextButtonClick(Object sender, RoutedEventArgs e)
        {
            string _Root;
            //Limux
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                _Root = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            }
            //Windows
            //if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            else
            {
                _Root = Directory.GetCurrentDirectory();
            }
            if (Directory.Exists(_Root + "/Client/") && sender.ToString() == "System.Windows.Controls.Button: Play")
            {
                if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                {
                    Console.WriteLine("Launching Windows Game");
                    System.Diagnostics.Process.Start(GameFile);
                    Environment.Exit(0);
                }
                if (Environment.OSVersion.Platform == PlatformID.Unix)
                {
                    Console.WriteLine("Launching Unix Game");
                    System.Diagnostics.Process.Start(_Root + "/Client/Linux.x86_64");
                    Environment.Exit(0);
                }
            }
            else
            {
                GameLauncher.Version localVersion = new GameLauncher.Version(File.ReadAllText(GameVersionFile));
                GameVersionText.Text = "Game Version: " + localVersion.ToString();
                try
                {
                    WebClient webClient = new WebClient();
                    GameLauncher.Version onlineVersion = new GameLauncher.Version(webClient.DownloadString(VersionAPI));

                    if (onlineVersion.IsDifferentThan(localVersion))
                    {
                        onlineVersion = new GameLauncher.Version(webClient.DownloadString(VersionAPI));

                        webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompleteCallBack);

                        //Linux
                        if (Environment.OSVersion.Platform == PlatformID.Unix)
                        {
                            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompleteCallBack);
                            webClient.DownloadFileAsync(new Uri(DownloadAPILinux), BuildFile, onlineVersion);
                        }
                        //Windows
                        if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                        {
                            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadGameCompleteCallBack);
                            webClient.DownloadFileAsync(new Uri(DownloadAPIWindows), BuildFile, onlineVersion);
                        }
                    }
                    else
                    {
                        //Status = LauncherStatus.ready;
                    }
                }
                catch (Exception ex)
                {
                    //Status = LauncherStatus.failed;
                    Console.WriteLine($"Failed to download content: {ex}");

                }
            }
        }

        void CloseButtonClick(Object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

    }


    public struct Version
    {
        internal static Version zero = new Version(0, 0, 0);
        public short major;
        public short minor;
        public short subMinor;


        internal Version(short _major, short _minor, short _subMinor)
        {
            major = _major;
            minor = _minor;
            subMinor = _subMinor;
        }
        internal Version(string _version)
        {
            string[] _versionStrings = _version.Split('.');
            if (_versionStrings.Length != 3)
            {
                major = 0;
                minor = 0;
                subMinor = 0;
                return;
            }
            major = short.Parse(_versionStrings[0]);
            minor = short.Parse(_versionStrings[1]);
            subMinor = short.Parse(_versionStrings[2]);
        }

        internal bool IsDifferentThan(Version _OtherVersion)
        {
            if (major != _OtherVersion.major)
            {
                return true;
            }
            else
            {
                if (minor != _OtherVersion.minor)
                {
                    return true;
                }
                else
                {
                    if (subMinor != _OtherVersion.subMinor)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override String ToString()
        {
            return $"{major}.{minor}.{subMinor}";
        }
    }
}