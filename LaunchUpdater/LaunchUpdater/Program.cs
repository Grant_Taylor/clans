﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaunchUpdater
{
    internal class Program
    {        
        static void Main(string[] args)
        {
            //Variables
            string LauncherAPI = "https://api.onedrive.com/v1.0/shares/u!aHR0cHM6Ly8xZHJ2Lm1zL3QvcyFBakl6RkhpenBQUWhqS2xkakY4ZHZxMmp0aXpiUEE_ZT1wcDkxVjE/root/content";
            string LauncherName = "ClansLauncher.exe";
            string RootDirectory = "";

            //Environment directory checks
            //Linux
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            {
                RootDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            }
            //Windows
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                RootDirectory = Directory.GetCurrentDirectory();
            }

            //Remove old Launcher
            string FileName = Path.Combine(RootDirectory, LauncherName);
            if(File.Exists(FileName))
            {
                try
                {
                    File.Delete(FileName);
                }
                catch (IOException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            //Initialise webClient to download new Launcher
            WebClient webClient = new WebClient();
            webClient.DownloadFile(new Uri(LauncherAPI), RootDirectory + LauncherName);

            //Wait until download is finished before launching Launcher
            System.Diagnostics.Process.Start(Path.Combine(RootDirectory, LauncherName));
        }
    }
}
