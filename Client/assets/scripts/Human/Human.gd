extends CharacterBody3D
@onready var camera_mount = $CameraMount
@onready var animation_player = $Rendering/mixamo_base/AnimationPlayer
@onready var rendering = $Rendering

var CurrentSpeed = 2.0
var CamMin = -1.3089969
var CamMax = 1.3962634
var CamRotX = 0.0
var CamRotY = 0.0
@export var WalkSpeed = 2.0
@export var SprintSpeed = 4.8
@export var JumpStrength = 4.5
@export var MouseSensitivity = 0.6

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y((deg_to_rad(-event.relative.x)) * MouseSensitivity)
		rendering.rotate_y(-(deg_to_rad(-event.relative.x)) * MouseSensitivity)
		camera_mount.rotate_x((deg_to_rad(-event.relative.y)) * MouseSensitivity)
		camera_mount.rotation.x = (clamp(camera_mount.rotation.x, CamMin, CamMax))

func _physics_process(delta):
	
	if Input.is_action_pressed("Run"):
		animation_player.play("running")
		CurrentSpeed = SprintSpeed
	else:
		animation_player.play("walking")
		CurrentSpeed = WalkSpeed
	CurrentSpeed * delta
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("Jump") and is_on_floor():
		velocity.y = JumpStrength

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("StrafeLeft", "StrafeRight", "Forward", "Backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		rendering.look_at(position + direction)
		velocity.x = direction.x * CurrentSpeed
		velocity.z = direction.z * CurrentSpeed
	else:
		if animation_player.current_animation != "idle":
			animation_player.play("idle")
		velocity.x = move_toward(velocity.x, 0, CurrentSpeed)
		velocity.z = move_toward(velocity.z, 0, CurrentSpeed)

	move_and_slide()
